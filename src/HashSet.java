
import java.io.Serializable;
import java.util.*;

public class HashSet<E> extends AbstractSet<E>
        implements Set<E>, Cloneable, Serializable {

    // add, contains, isEmpty, iterator, remove, size

    private Object objectPresent = new Object();


    private transient HashMap<E, Object> map;

    public HashSet() {
        map = new HashMap<E, Object>();
    }


    public boolean add(E element) {
        if (contains(element)) {
            return false;
        } else {
            map.put(element, objectPresent);
            return true;

        }

    }

    public boolean contains(Object key) {
        return map.containsKey(key);
    }

    public boolean isEmpty() {
        if (map.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Iterator<E> iterator() {
        return map.keySet().iterator();
    }


    public boolean remove(Object key){
        if(contains(key)){
            map.remove(key);
            return true;
        }
        else{return false;}

    }

    @Override
    public int size() {
        return map.size();
    }

    public static void main(String[] args) {

        HashSet<Integer> hashSet=new HashSet<>();

        hashSet.add(23);
        hashSet.add(18);
        hashSet.add(23);
        hashSet.add(67);
        hashSet.add(88);
        hashSet.add(99);

        for(Integer s:hashSet)
        {
            System.out.println(s); //18 67 99 23 88
        }
        System.out.println();

        hashSet.remove(88);


        for(Integer s:hashSet)
        {
            System.out.println(s); //18 67 99 23
        }
        System.out.println();

        Iterator value = hashSet.iterator();
        while(value.hasNext())
        {
            System.out.println(value.next());
        }

        System.out.println();


        System.out.println(hashSet.size()); //4
        System.out.println();

        System.out.println(hashSet.contains(23)); //true
        System.out.println(hashSet.contains(99)); //false

        System.out.println();
        System.out.println(hashSet.isEmpty()); //false



    }


}
